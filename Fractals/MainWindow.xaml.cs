﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fractals
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_Start(object sender, RoutedEventArgs e)
        {
            var nums = Enumerable.Range(1, 100).ToList();

            int prevX = 0;
            int prevY = 0;

            for (int i = 0; i < nums.Count; i++)
            {
                Line line = new Line();
                line.Stroke = SystemColors.WindowFrameBrush;
                line.X1 = prevX;
                line.Y1 = prevY;
                line.X2 = prevX = i;
                line.Y2 = prevY = Draw(i);

                

                myCanvas.Children.Add(line);
            }

            
        }

        private void Button_Click_Stop(object sender, RoutedEventArgs e)
        {
            myCanvas.Children.Clear();
        }

        private int Draw(int x)
        {
            return x * x + 3;
        }
    }
}
