﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EulerProject
{
    class Solver
    {
        public bool IsCircularPrime(int number)
        {
            int counter = 0;
            if (IsPrime(number))
                counter++;
            else
                return false;

            int digitsCount = number.ToString().Length;
            for (int i = 0; i < digitsCount-1; i++)
            {
                var d = number / 10;
                number = d + (int)Math.Pow(10, digitsCount - 1) * (number % 10);
                if (IsPrime(number))
                    counter++;
                else
                    return false;
            }

            var res =  counter == digitsCount;

            return res;
        }
        
        public bool IsPrime(int number)
        {
            if (number == 1 || number % 2 == 0 || number % 3 == 0 || number % 5 == 0)
                return false;

            for (int i = 3; i < number-1; i+=2)
            {
                if (number % i == 0)
                {
                    return false;
                }
            }

            return true;           
        }
    }
}
